package com.weroko.caproduct.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.weroko.caproduct.dto.AuditorDetailsDto;
import com.weroko.caproduct.dto.CAccountSettingsDto;
import com.weroko.caproduct.dto.CompanyAddressDto;
import com.weroko.caproduct.dto.CompanyInfoDto;
import com.weroko.caproduct.dto.CompanyRegistrationDto;
import com.weroko.caproduct.dto.SignatoriesDetailsDto;
import com.weroko.caproduct.entity.CAccountSettings;
import com.weroko.caproduct.entity.CompanyAddress;
import com.weroko.caproduct.model.CompanyAddressModel;
import com.weroko.caproduct.service.CreateCompanyService;


@RestController
@RequestMapping("/api/createcompany")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class CreateCompanyController {
	
	
	@Autowired
	CreateCompanyService createCompanyService;
	
	
	
	

	@RequestMapping(value = "/addCompanyInfo/{user_id}", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<CompanyInfoDto> addCompanyInfo(@RequestBody CompanyInfoDto companyInfoDto, @PathVariable int user_id)
	{
		CompanyInfoDto companyInfoDto2 = createCompanyService.addCompanyInfo(companyInfoDto, user_id);
		return new ResponseEntity<CompanyInfoDto>(companyInfoDto2, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/addCompanyAddress", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<CompanyAddressModel> addCompanyAddress(@RequestBody CompanyAddressModel companyAddressModel)
	{
		CompanyAddressModel companyAddressModel2 =createCompanyService.addCompanyAddress(companyAddressModel);
		return new ResponseEntity<CompanyAddressModel>(companyAddressModel2, HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/addCompanySettings/{company_id}", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<String> addCompanySettings(@RequestBody CAccountSettingsDto cAccountSettingsDto, @PathVariable int company_id)
	{
		String message=createCompanyService.addCompanySettings(cAccountSettingsDto,company_id);
		return new ResponseEntity<String>(message, HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/addSignatoriesDetails/{company_id}", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<String> addSignatoriesDetails(@RequestBody SignatoriesDetailsDto signatoriesDetailsDto, @PathVariable int company_id)
	{
		String message=createCompanyService.addSignatoriesDetails(signatoriesDetailsDto,company_id);
		return new ResponseEntity<String>(message, HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/addAuditorDetails/{company_id}", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<String> addAuditorDetails(@RequestBody AuditorDetailsDto auditorDetailsDto, @PathVariable int company_id)
	{
		String message=createCompanyService.addAuditorDetails(auditorDetailsDto,company_id);
		return new ResponseEntity<String>(message, HttpStatus.OK);
		
	}
	
	

}
