package com.weroko.caproduct.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.weroko.caproduct.config.JwtTokenUtil;
import com.weroko.caproduct.dto.CompanyRegistrationDto;
import com.weroko.caproduct.dto.UserInfo;
import com.weroko.caproduct.entity.JwtRequest;
import com.weroko.caproduct.entity.JwtResponse;
import com.weroko.caproduct.exception.exception_handler.ResponseMsg;
import com.weroko.caproduct.service.HomeControllerService;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class HomeController {
	
	@Autowired
	private HomeControllerService homeControllerService;
	
	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Autowired
	private JwtTokenUtil jwtTokenUtil;
	

	
	@RequestMapping(value = "/registerUser", method = RequestMethod.POST)
	public ResponseEntity<ResponseMsg> registerUser(@RequestBody CompanyRegistrationDto companyRegistrationDto)
	{
		ResponseMsg responseMsg = new ResponseMsg();
		String status= homeControllerService.registerUser(companyRegistrationDto);
		
		responseMsg.setMessage(status);
		responseMsg.setStatus("Registered Successfully!");
		responseMsg.setGenId(1);
		return new ResponseEntity<ResponseMsg>(responseMsg, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
	public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) throws Exception {
		
		authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());

		final UserDetails userDetails = homeControllerService
				.loadUserByUsername(authenticationRequest.getUsername());

		final String token = jwtTokenUtil.generateToken(userDetails);

		return ResponseEntity.ok(new JwtResponse(token));
	}
	
	private void authenticate(String username, String password) throws Exception {
		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
		} catch (DisabledException e) {
			throw new Exception("USER_DISABLED", e);
		} catch (BadCredentialsException e) {
			throw new Exception("INVALID_CREDENTIALS", e);
		}
	}
	
	
	@RequestMapping(value = "/userinfo/{username}", method = RequestMethod.GET)
	public ResponseEntity<UserInfo> getAllUserDetails(@PathVariable String username){
		UserInfo userInfo = homeControllerService.getAllUserDetails(username);
		return new ResponseEntity<UserInfo>(userInfo, HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/testme", method = RequestMethod.GET)
	public ResponseEntity<ResponseMsg> testMe(){
		ResponseMsg resp = new ResponseMsg();
		System.out.println("called me");
		resp.setStatus("Success");
		resp.setMessage("got u");
		return new ResponseEntity<ResponseMsg>(resp, HttpStatus.OK);
		
	}


}
