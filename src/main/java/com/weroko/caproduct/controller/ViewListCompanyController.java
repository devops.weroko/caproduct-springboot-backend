package com.weroko.caproduct.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.weroko.caproduct.dto.UserInfo;
import com.weroko.caproduct.entity.CompanyAddress;
import com.weroko.caproduct.entity.CompanyInfo;
import com.weroko.caproduct.model.CompanyAddressModel;
import com.weroko.caproduct.model.CompanyInfoModel;
import com.weroko.caproduct.service.ViewListCompanyService;



@RestController
@RequestMapping("/api/viewlistcompany")
@CrossOrigin(origins = "*")
public class ViewListCompanyController {
	
	@Autowired
	ViewListCompanyService viewListCompanyService;
	
	@GetMapping("/listcompany/{user_id}")
	public ResponseEntity<List<CompanyInfo>> getCompanyList (@PathVariable int user_id)
	{
		System.out.println("checkme"+user_id);
		List<CompanyInfo> companyInfos=viewListCompanyService.getCompanyList(user_id);
		return new ResponseEntity<List<CompanyInfo>>(companyInfos,HttpStatus.OK);
	}
	
	// /companyinfo/{company_id}
	
	@GetMapping("/companyInfo/{company_id}")
	public ResponseEntity<CompanyInfoModel> getCompanyInfo (@PathVariable int company_id)
	{
		
		CompanyInfoModel companyInfoModel = viewListCompanyService.getCompanyInfo(company_id);
		return new ResponseEntity<CompanyInfoModel>(companyInfoModel,HttpStatus.OK);	
		}
	
	
	@GetMapping("/companyAddress/{company_id}")
	public ResponseEntity<CompanyAddressModel> getCompanyAddress (@PathVariable int company_id)
	{
		
		CompanyAddressModel companyAddressModel =viewListCompanyService.getCompanyAddress(company_id);
		return new ResponseEntity<CompanyAddressModel>(companyAddressModel,HttpStatus.OK);	
		}

}
