package com.weroko.caproduct.exception.exception_handler;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.weroko.caproduct.exception.ApplicationException;;

@RestControllerAdvice
public class ExceptionHandlerClass {
	
	@ExceptionHandler(ApplicationException.class)
	  public ResponseMsg handleNotFoundException(ApplicationException ex, HttpServletRequest request) {
		 
	   ResponseMsg responseMsg= new ResponseMsg();
	    responseMsg.setMessage(ex.getMessage());
	    responseMsg.setStatus("Error");
	    return responseMsg;
	 }
	
	
	
	

}

