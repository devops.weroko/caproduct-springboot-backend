package com.weroko.caproduct.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
@Entity
public class CAccountSettings {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	private String date_format;
	
	private String base_country;
	
	private String currency_symbol;
	
	private String currency_decimal_place;
	
	private String user_id;
	
	private String password;
	
	@OneToOne
	@JoinColumn(name ="company_id")
	private CompanyInfo companyInfo;

	public CAccountSettings() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CAccountSettings(int id, String date_format, String base_country, String currency_symbol,
			String currency_decimal_place, String user_id, String password, CompanyInfo companyInfo) {
		super();
		this.id = id;
		this.date_format = date_format;
		this.base_country = base_country;
		this.currency_symbol = currency_symbol;
		this.currency_decimal_place = currency_decimal_place;
		this.user_id = user_id;
		this.password = password;
		this.companyInfo = companyInfo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDate_format() {
		return date_format;
	}

	public void setDate_format(String date_format) {
		this.date_format = date_format;
	}

	public String getBase_country() {
		return base_country;
	}

	public void setBase_country(String base_country) {
		this.base_country = base_country;
	}

	public String getCurrency_symbol() {
		return currency_symbol;
	}

	public void setCurrency_symbol(String currency_symbol) {
		this.currency_symbol = currency_symbol;
	}

	public String getCurrency_decimal_place() {
		return currency_decimal_place;
	}

	public void setCurrency_decimal_place(String currency_decimal_place) {
		this.currency_decimal_place = currency_decimal_place;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public CompanyInfo getCompanyInfo() {
		return companyInfo;
	}

	public void setCompanyInfo(CompanyInfo companyInfo) {
		this.companyInfo = companyInfo;
	}

	@Override
	public String toString() {
		return "CAccountSettings [id=" + id + ", date_format=" + date_format + ", base_country=" + base_country
				+ ", currency_symbol=" + currency_symbol + ", currency_decimal_place=" + currency_decimal_place
				+ ", user_id=" + user_id + ", password=" + password + ", companyInfo=" + companyInfo + "]";
	}

	

}
