package com.weroko.caproduct.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
@Entity
public class CompanyBranch {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int branch_id;
	
	private String branch_name;
	
	private String branch_code;
	
	private String street;
	
	private String city_town;
	
	private String police_station;
	
	private String district;
	
	private String state;
	
	private String country;
	
	private String zip_code;
	
	private String attachment;
	
	private String trade_liense_no;
	
	private String tl_effective_date;
	
	private String tl_department_address;
	
	private String tl_rang_ward;
	
	private String tl_telephone_no;
	
	private String tl_attachment;
	
	private String ptax_enrollment_no;
	
	private String ptax_effective_date;
	
	private String ptax_department_address;
	
	private String ptax_range_ward;
	
	private String ptax_telephone_no;
	
	private String ptax_attachment;
	
	private String gst_no;
	
	private String gst_effective_date;
	
	private String gst_address;
	
	private String gst_range_ward;
	
	private String gst_telephone_no;
	
	private String gst_attachment;
	
	private String tan_deduct_collect_no;
	
	private String tan_effective_date;
	
	private String tan_address;
	
	private String tan_range_ward;
	
	private String tan_telephone_no;
	
	private String tan_attachment;
	
	private String esi_no;
	
	private String esi_effective_date;
	
	private String esi_address;
	
	private String esi_range_ward;
	
	private String esi_telephone_no;
	
	private String esi_attachment;
	
	private String other_registration_no;
	
	private String other_effective_no;
	
	private String other_address;
	
	private String other_range_ward;
	
	private String other_telephone_no;
	
	private String other_attachments;
	
	
	@ManyToOne
	@JoinColumn(name = "company_id")
	private CompanyInfo companyInfo;


	public CompanyBranch() {
		super();
		// TODO Auto-generated constructor stub
	}


	public CompanyBranch(int branch_id, String branch_name, String branch_code, String street, String city_town,
			String police_station, String district, String state, String country, String zip_code, String attachment,
			String trade_liense_no, String tl_effective_date, String tl_department_address, String tl_rang_ward,
			String tl_telephone_no, String tl_attachment, String ptax_enrollment_no, String ptax_effective_date,
			String ptax_department_address, String ptax_range_ward, String ptax_telephone_no, String ptax_attachment,
			String gst_no, String gst_effective_date, String gst_address, String gst_range_ward,
			String gst_telephone_no, String gst_attachment, String tan_deduct_collect_no, String tan_effective_date,
			String tan_address, String tan_range_ward, String tan_telephone_no, String tan_attachment, String esi_no,
			String esi_effective_date, String esi_address, String esi_range_ward, String esi_telephone_no,
			String esi_attachment, String other_registration_no, String other_effective_no, String other_address,
			String other_range_ward, String other_telephone_no, String other_attachments, CompanyInfo companyInfo) {
		super();
		this.branch_id = branch_id;
		this.branch_name = branch_name;
		this.branch_code = branch_code;
		this.street = street;
		this.city_town = city_town;
		this.police_station = police_station;
		this.district = district;
		this.state = state;
		this.country = country;
		this.zip_code = zip_code;
		this.attachment = attachment;
		this.trade_liense_no = trade_liense_no;
		this.tl_effective_date = tl_effective_date;
		this.tl_department_address = tl_department_address;
		this.tl_rang_ward = tl_rang_ward;
		this.tl_telephone_no = tl_telephone_no;
		this.tl_attachment = tl_attachment;
		this.ptax_enrollment_no = ptax_enrollment_no;
		this.ptax_effective_date = ptax_effective_date;
		this.ptax_department_address = ptax_department_address;
		this.ptax_range_ward = ptax_range_ward;
		this.ptax_telephone_no = ptax_telephone_no;
		this.ptax_attachment = ptax_attachment;
		this.gst_no = gst_no;
		this.gst_effective_date = gst_effective_date;
		this.gst_address = gst_address;
		this.gst_range_ward = gst_range_ward;
		this.gst_telephone_no = gst_telephone_no;
		this.gst_attachment = gst_attachment;
		this.tan_deduct_collect_no = tan_deduct_collect_no;
		this.tan_effective_date = tan_effective_date;
		this.tan_address = tan_address;
		this.tan_range_ward = tan_range_ward;
		this.tan_telephone_no = tan_telephone_no;
		this.tan_attachment = tan_attachment;
		this.esi_no = esi_no;
		this.esi_effective_date = esi_effective_date;
		this.esi_address = esi_address;
		this.esi_range_ward = esi_range_ward;
		this.esi_telephone_no = esi_telephone_no;
		this.esi_attachment = esi_attachment;
		this.other_registration_no = other_registration_no;
		this.other_effective_no = other_effective_no;
		this.other_address = other_address;
		this.other_range_ward = other_range_ward;
		this.other_telephone_no = other_telephone_no;
		this.other_attachments = other_attachments;
		this.companyInfo = companyInfo;
	}


	public int getBranch_id() {
		return branch_id;
	}


	public void setBranch_id(int branch_id) {
		this.branch_id = branch_id;
	}


	public String getBranch_name() {
		return branch_name;
	}


	public void setBranch_name(String branch_name) {
		this.branch_name = branch_name;
	}


	public String getBranch_code() {
		return branch_code;
	}


	public void setBranch_code(String branch_code) {
		this.branch_code = branch_code;
	}


	public String getStreet() {
		return street;
	}


	public void setStreet(String street) {
		this.street = street;
	}


	public String getCity_town() {
		return city_town;
	}


	public void setCity_town(String city_town) {
		this.city_town = city_town;
	}


	public String getPolice_station() {
		return police_station;
	}


	public void setPolice_station(String police_station) {
		this.police_station = police_station;
	}


	public String getDistrict() {
		return district;
	}


	public void setDistrict(String district) {
		this.district = district;
	}


	public String getState() {
		return state;
	}


	public void setState(String state) {
		this.state = state;
	}


	public String getCountry() {
		return country;
	}


	public void setCountry(String country) {
		this.country = country;
	}


	public String getZip_code() {
		return zip_code;
	}


	public void setZip_code(String zip_code) {
		this.zip_code = zip_code;
	}


	public String getAttachment() {
		return attachment;
	}


	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}


	public String getTrade_liense_no() {
		return trade_liense_no;
	}


	public void setTrade_liense_no(String trade_liense_no) {
		this.trade_liense_no = trade_liense_no;
	}


	public String getTl_effective_date() {
		return tl_effective_date;
	}


	public void setTl_effective_date(String tl_effective_date) {
		this.tl_effective_date = tl_effective_date;
	}


	public String getTl_department_address() {
		return tl_department_address;
	}


	public void setTl_department_address(String tl_department_address) {
		this.tl_department_address = tl_department_address;
	}


	public String getTl_rang_ward() {
		return tl_rang_ward;
	}


	public void setTl_rang_ward(String tl_rang_ward) {
		this.tl_rang_ward = tl_rang_ward;
	}


	public String getTl_telephone_no() {
		return tl_telephone_no;
	}


	public void setTl_telephone_no(String tl_telephone_no) {
		this.tl_telephone_no = tl_telephone_no;
	}


	public String getTl_attachment() {
		return tl_attachment;
	}


	public void setTl_attachment(String tl_attachment) {
		this.tl_attachment = tl_attachment;
	}


	public String getPtax_enrollment_no() {
		return ptax_enrollment_no;
	}


	public void setPtax_enrollment_no(String ptax_enrollment_no) {
		this.ptax_enrollment_no = ptax_enrollment_no;
	}


	public String getPtax_effective_date() {
		return ptax_effective_date;
	}


	public void setPtax_effective_date(String ptax_effective_date) {
		this.ptax_effective_date = ptax_effective_date;
	}


	public String getPtax_department_address() {
		return ptax_department_address;
	}


	public void setPtax_department_address(String ptax_department_address) {
		this.ptax_department_address = ptax_department_address;
	}


	public String getPtax_range_ward() {
		return ptax_range_ward;
	}


	public void setPtax_range_ward(String ptax_range_ward) {
		this.ptax_range_ward = ptax_range_ward;
	}


	public String getPtax_telephone_no() {
		return ptax_telephone_no;
	}


	public void setPtax_telephone_no(String ptax_telephone_no) {
		this.ptax_telephone_no = ptax_telephone_no;
	}


	public String getPtax_attachment() {
		return ptax_attachment;
	}


	public void setPtax_attachment(String ptax_attachment) {
		this.ptax_attachment = ptax_attachment;
	}


	public String getGst_no() {
		return gst_no;
	}


	public void setGst_no(String gst_no) {
		this.gst_no = gst_no;
	}


	public String getGst_effective_date() {
		return gst_effective_date;
	}


	public void setGst_effective_date(String gst_effective_date) {
		this.gst_effective_date = gst_effective_date;
	}


	public String getGst_address() {
		return gst_address;
	}


	public void setGst_address(String gst_address) {
		this.gst_address = gst_address;
	}


	public String getGst_range_ward() {
		return gst_range_ward;
	}


	public void setGst_range_ward(String gst_range_ward) {
		this.gst_range_ward = gst_range_ward;
	}


	public String getGst_telephone_no() {
		return gst_telephone_no;
	}


	public void setGst_telephone_no(String gst_telephone_no) {
		this.gst_telephone_no = gst_telephone_no;
	}


	public String getGst_attachment() {
		return gst_attachment;
	}


	public void setGst_attachment(String gst_attachment) {
		this.gst_attachment = gst_attachment;
	}


	public String getTan_deduct_collect_no() {
		return tan_deduct_collect_no;
	}


	public void setTan_deduct_collect_no(String tan_deduct_collect_no) {
		this.tan_deduct_collect_no = tan_deduct_collect_no;
	}


	public String getTan_effective_date() {
		return tan_effective_date;
	}


	public void setTan_effective_date(String tan_effective_date) {
		this.tan_effective_date = tan_effective_date;
	}


	public String getTan_address() {
		return tan_address;
	}


	public void setTan_address(String tan_address) {
		this.tan_address = tan_address;
	}


	public String getTan_range_ward() {
		return tan_range_ward;
	}


	public void setTan_range_ward(String tan_range_ward) {
		this.tan_range_ward = tan_range_ward;
	}


	public String getTan_telephone_no() {
		return tan_telephone_no;
	}


	public void setTan_telephone_no(String tan_telephone_no) {
		this.tan_telephone_no = tan_telephone_no;
	}


	public String getTan_attachment() {
		return tan_attachment;
	}


	public void setTan_attachment(String tan_attachment) {
		this.tan_attachment = tan_attachment;
	}


	public String getEsi_no() {
		return esi_no;
	}


	public void setEsi_no(String esi_no) {
		this.esi_no = esi_no;
	}


	public String getEsi_effective_date() {
		return esi_effective_date;
	}


	public void setEsi_effective_date(String esi_effective_date) {
		this.esi_effective_date = esi_effective_date;
	}


	public String getEsi_address() {
		return esi_address;
	}


	public void setEsi_address(String esi_address) {
		this.esi_address = esi_address;
	}


	public String getEsi_range_ward() {
		return esi_range_ward;
	}


	public void setEsi_range_ward(String esi_range_ward) {
		this.esi_range_ward = esi_range_ward;
	}


	public String getEsi_telephone_no() {
		return esi_telephone_no;
	}


	public void setEsi_telephone_no(String esi_telephone_no) {
		this.esi_telephone_no = esi_telephone_no;
	}


	public String getEsi_attachment() {
		return esi_attachment;
	}


	public void setEsi_attachment(String esi_attachment) {
		this.esi_attachment = esi_attachment;
	}


	public String getOther_registration_no() {
		return other_registration_no;
	}


	public void setOther_registration_no(String other_registration_no) {
		this.other_registration_no = other_registration_no;
	}


	public String getOther_effective_no() {
		return other_effective_no;
	}


	public void setOther_effective_no(String other_effective_no) {
		this.other_effective_no = other_effective_no;
	}


	public String getOther_address() {
		return other_address;
	}


	public void setOther_address(String other_address) {
		this.other_address = other_address;
	}


	public String getOther_range_ward() {
		return other_range_ward;
	}


	public void setOther_range_ward(String other_range_ward) {
		this.other_range_ward = other_range_ward;
	}


	public String getOther_telephone_no() {
		return other_telephone_no;
	}


	public void setOther_telephone_no(String other_telephone_no) {
		this.other_telephone_no = other_telephone_no;
	}


	public String getOther_attachments() {
		return other_attachments;
	}


	public void setOther_attachments(String other_attachments) {
		this.other_attachments = other_attachments;
	}


	public CompanyInfo getCompanyInfo() {
		return companyInfo;
	}


	public void setCompanyInfo(CompanyInfo companyInfo) {
		this.companyInfo = companyInfo;
	}


	@Override
	public String toString() {
		return "CompanyBranch [branch_id=" + branch_id + ", branch_name=" + branch_name + ", branch_code=" + branch_code
				+ ", street=" + street + ", city_town=" + city_town + ", police_station=" + police_station
				+ ", district=" + district + ", state=" + state + ", country=" + country + ", zip_code=" + zip_code
				+ ", attachment=" + attachment + ", trade_liense_no=" + trade_liense_no + ", tl_effective_date="
				+ tl_effective_date + ", tl_department_address=" + tl_department_address + ", tl_rang_ward="
				+ tl_rang_ward + ", tl_telephone_no=" + tl_telephone_no + ", tl_attachment=" + tl_attachment
				+ ", ptax_enrollment_no=" + ptax_enrollment_no + ", ptax_effective_date=" + ptax_effective_date
				+ ", ptax_department_address=" + ptax_department_address + ", ptax_range_ward=" + ptax_range_ward
				+ ", ptax_telephone_no=" + ptax_telephone_no + ", ptax_attachment=" + ptax_attachment + ", gst_no="
				+ gst_no + ", gst_effective_date=" + gst_effective_date + ", gst_address=" + gst_address
				+ ", gst_range_ward=" + gst_range_ward + ", gst_telephone_no=" + gst_telephone_no + ", gst_attachment="
				+ gst_attachment + ", tan_deduct_collect_no=" + tan_deduct_collect_no + ", tan_effective_date="
				+ tan_effective_date + ", tan_address=" + tan_address + ", tan_range_ward=" + tan_range_ward
				+ ", tan_telephone_no=" + tan_telephone_no + ", tan_attachment=" + tan_attachment + ", esi_no=" + esi_no
				+ ", esi_effective_date=" + esi_effective_date + ", esi_address=" + esi_address + ", esi_range_ward="
				+ esi_range_ward + ", esi_telephone_no=" + esi_telephone_no + ", esi_attachment=" + esi_attachment
				+ ", other_registration_no=" + other_registration_no + ", other_effective_no=" + other_effective_no
				+ ", other_address=" + other_address + ", other_range_ward=" + other_range_ward
				+ ", other_telephone_no=" + other_telephone_no + ", other_attachments=" + other_attachments
				+ ", companyInfo=" + companyInfo + "]";
	}
	
	

	
	
	

}
