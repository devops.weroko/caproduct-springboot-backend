package com.weroko.caproduct.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
@Entity
public class CompanyWarehouse {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	private String warehouse_name;
	
	private String warehouse_code;
	
	private String street;
	
	private String area;
	
	private String city_town;
	
	private String police_station;
	
	private String district;
	
	private String state;
	
	private String country;
	
	private String zip_code;
	
	private String attachment;
	
	private String tl_no;
	
	private String tl_effective_date;
	
	private String tl_address;
	
	private String tl_range_ward;
	
	private String tl_telephone_no;
	
	private String tl_attachment;
	
	private String ptax_enrollment_no;
	
	private String ptax_effective_date;
	
	private String ptax_address;
	
	private String ptax_range_ward;
	
	private String ptax_telephone_no;
	
	private String ptax_attachment;
	
	
	@ManyToOne
	@JoinColumn(name = "company_id")
	private CompanyInfo companyInfo;


	public CompanyWarehouse() {
		super();
		// TODO Auto-generated constructor stub
	}


	public CompanyWarehouse(int id, String warehouse_name, String warehouse_code, String street, String area,
			String city_town, String police_station, String district, String state, String country, String zip_code,
			String attachment, String tl_no, String tl_effective_date, String tl_address, String tl_range_ward,
			String tl_telephone_no, String tl_attachment, String ptax_enrollment_no, String ptax_effective_date,
			String ptax_address, String ptax_range_ward, String ptax_telephone_no, String ptax_attachment,
			CompanyInfo companyInfo) {
		super();
		this.id = id;
		this.warehouse_name = warehouse_name;
		this.warehouse_code = warehouse_code;
		this.street = street;
		this.area = area;
		this.city_town = city_town;
		this.police_station = police_station;
		this.district = district;
		this.state = state;
		this.country = country;
		this.zip_code = zip_code;
		this.attachment = attachment;
		this.tl_no = tl_no;
		this.tl_effective_date = tl_effective_date;
		this.tl_address = tl_address;
		this.tl_range_ward = tl_range_ward;
		this.tl_telephone_no = tl_telephone_no;
		this.tl_attachment = tl_attachment;
		this.ptax_enrollment_no = ptax_enrollment_no;
		this.ptax_effective_date = ptax_effective_date;
		this.ptax_address = ptax_address;
		this.ptax_range_ward = ptax_range_ward;
		this.ptax_telephone_no = ptax_telephone_no;
		this.ptax_attachment = ptax_attachment;
		this.companyInfo = companyInfo;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getWarehouse_name() {
		return warehouse_name;
	}


	public void setWarehouse_name(String warehouse_name) {
		this.warehouse_name = warehouse_name;
	}


	public String getWarehouse_code() {
		return warehouse_code;
	}


	public void setWarehouse_code(String warehouse_code) {
		this.warehouse_code = warehouse_code;
	}


	public String getStreet() {
		return street;
	}


	public void setStreet(String street) {
		this.street = street;
	}


	public String getArea() {
		return area;
	}


	public void setArea(String area) {
		this.area = area;
	}


	public String getCity_town() {
		return city_town;
	}


	public void setCity_town(String city_town) {
		this.city_town = city_town;
	}


	public String getPolice_station() {
		return police_station;
	}


	public void setPolice_station(String police_station) {
		this.police_station = police_station;
	}


	public String getDistrict() {
		return district;
	}


	public void setDistrict(String district) {
		this.district = district;
	}


	public String getState() {
		return state;
	}


	public void setState(String state) {
		this.state = state;
	}


	public String getCountry() {
		return country;
	}


	public void setCountry(String country) {
		this.country = country;
	}


	public String getZip_code() {
		return zip_code;
	}


	public void setZip_code(String zip_code) {
		this.zip_code = zip_code;
	}


	public String getAttachment() {
		return attachment;
	}


	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}


	public String getTl_no() {
		return tl_no;
	}


	public void setTl_no(String tl_no) {
		this.tl_no = tl_no;
	}


	public String getTl_effective_date() {
		return tl_effective_date;
	}


	public void setTl_effective_date(String tl_effective_date) {
		this.tl_effective_date = tl_effective_date;
	}


	public String getTl_address() {
		return tl_address;
	}


	public void setTl_address(String tl_address) {
		this.tl_address = tl_address;
	}


	public String getTl_range_ward() {
		return tl_range_ward;
	}


	public void setTl_range_ward(String tl_range_ward) {
		this.tl_range_ward = tl_range_ward;
	}


	public String getTl_telephone_no() {
		return tl_telephone_no;
	}


	public void setTl_telephone_no(String tl_telephone_no) {
		this.tl_telephone_no = tl_telephone_no;
	}


	public String getTl_attachment() {
		return tl_attachment;
	}


	public void setTl_attachment(String tl_attachment) {
		this.tl_attachment = tl_attachment;
	}


	public String getPtax_enrollment_no() {
		return ptax_enrollment_no;
	}


	public void setPtax_enrollment_no(String ptax_enrollment_no) {
		this.ptax_enrollment_no = ptax_enrollment_no;
	}


	public String getPtax_effective_date() {
		return ptax_effective_date;
	}


	public void setPtax_effective_date(String ptax_effective_date) {
		this.ptax_effective_date = ptax_effective_date;
	}


	public String getPtax_address() {
		return ptax_address;
	}


	public void setPtax_address(String ptax_address) {
		this.ptax_address = ptax_address;
	}


	public String getPtax_range_ward() {
		return ptax_range_ward;
	}


	public void setPtax_range_ward(String ptax_range_ward) {
		this.ptax_range_ward = ptax_range_ward;
	}


	public String getPtax_telephone_no() {
		return ptax_telephone_no;
	}


	public void setPtax_telephone_no(String ptax_telephone_no) {
		this.ptax_telephone_no = ptax_telephone_no;
	}


	public String getPtax_attachment() {
		return ptax_attachment;
	}


	public void setPtax_attachment(String ptax_attachment) {
		this.ptax_attachment = ptax_attachment;
	}


	public CompanyInfo getCompanyInfo() {
		return companyInfo;
	}


	public void setCompanyInfo(CompanyInfo companyInfo) {
		this.companyInfo = companyInfo;
	}


	@Override
	public String toString() {
		return "CompanyWarehouse [id=" + id + ", warehouse_name=" + warehouse_name + ", warehouse_code="
				+ warehouse_code + ", street=" + street + ", area=" + area + ", city_town=" + city_town
				+ ", police_station=" + police_station + ", district=" + district + ", state=" + state + ", country="
				+ country + ", zip_code=" + zip_code + ", attachment=" + attachment + ", tl_no=" + tl_no
				+ ", tl_effective_date=" + tl_effective_date + ", tl_address=" + tl_address + ", tl_range_ward="
				+ tl_range_ward + ", tl_telephone_no=" + tl_telephone_no + ", tl_attachment=" + tl_attachment
				+ ", ptax_enrollment_no=" + ptax_enrollment_no + ", ptax_effective_date=" + ptax_effective_date
				+ ", ptax_address=" + ptax_address + ", ptax_range_ward=" + ptax_range_ward + ", ptax_telephone_no="
				+ ptax_telephone_no + ", ptax_attachment=" + ptax_attachment + ", companyInfo=" + companyInfo + "]";
	}
	
	
	




}
