
package com.weroko.caproduct.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class CompanyInfo {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int company_id;

	private String name;

	private String trade_name;

	private String pan;

	private String pan_effective_date;

	private String pan_dep_address;

	private String range_ward;

	private String pan_ph_no;

	private String pan_attachment;

	private String org_type;

	private String business_type;

	// private String industry_type;

	private String company_crn_no;

	private String crn_eff_date;

	private String crn_address;

	private String crn_range_ward;

	private String crn_ph_no;

	private String crn_attachment;

	private String company_llpin_no;

	private String llpin_eff_date;

	private String llpin_address;

	private String llpin_range_ward;

	private String llpin_ph_no;

	private String llpin_attachment;

	private String company_cin_no;

	private String cin_eff_date;

	private String cin_address;

	private String cin_range_ward;

	private String cin_ph_no;

	private String cin_attachment;

	// private String msme;

	private String company_logo;

	private String phone;

	private String mobile;

	private String email;

	private String website;

	private String date_of_incorporation;

	private String financial_year_start;

	private String book_commencing;

	private String commencement_date;

//	@OneToOne
//	private CAccountSettings cAccountSettings;
//	@OneToOne
//	private CompanyAddress companyAddress;
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "user_id")
	private CompanyRegistration companyRegistration;
	
	@JsonIgnore
	@OneToMany(mappedBy = "companyInfo")
	private List<CompanyBranch> companyBranch;

	@JsonIgnore
	@OneToMany(mappedBy = "companyInfo")
	private List<CompanyWarehouse> companyWarehouse;
	
	@JsonIgnore
	@OneToMany(mappedBy = "companyInfo")
	private List<SignatoriesDetails> signatoriesDetails;
	


	public CompanyInfo() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CompanyInfo(int company_id, String name, String trade_name, String pan, String pan_effective_date,
			String pan_dep_address, String range_ward, String pan_ph_no, String pan_attachment, String org_type,
			String business_type, String company_crn_no, String crn_eff_date, String crn_address, String crn_range_ward,
			String crn_ph_no, String crn_attachment, String company_llpin_no, String llpin_eff_date,
			String llpin_address, String llpin_range_ward, String llpin_ph_no, String llpin_attachment,
			String company_cin_no, String cin_eff_date, String cin_address, String cin_range_ward, String cin_ph_no,
			String cin_attachment, String company_logo, String phone, String mobile, String email, String website,
			String date_of_incorporation, String financial_year_start, String book_commencing, String commencement_date,
			CompanyRegistration companyRegistration, List<CompanyBranch> companyBranch,
			List<CompanyWarehouse> companyWarehouse) {
		super();
		this.company_id = company_id;
		this.name = name;
		this.trade_name = trade_name;
		this.pan = pan;
		this.pan_effective_date = pan_effective_date;
		this.pan_dep_address = pan_dep_address;
		this.range_ward = range_ward;
		this.pan_ph_no = pan_ph_no;
		this.pan_attachment = pan_attachment;
		this.org_type = org_type;
		this.business_type = business_type;
		this.company_crn_no = company_crn_no;
		this.crn_eff_date = crn_eff_date;
		this.crn_address = crn_address;
		this.crn_range_ward = crn_range_ward;
		this.crn_ph_no = crn_ph_no;
		this.crn_attachment = crn_attachment;
		this.company_llpin_no = company_llpin_no;
		this.llpin_eff_date = llpin_eff_date;
		this.llpin_address = llpin_address;
		this.llpin_range_ward = llpin_range_ward;
		this.llpin_ph_no = llpin_ph_no;
		this.llpin_attachment = llpin_attachment;
		this.company_cin_no = company_cin_no;
		this.cin_eff_date = cin_eff_date;
		this.cin_address = cin_address;
		this.cin_range_ward = cin_range_ward;
		this.cin_ph_no = cin_ph_no;
		this.cin_attachment = cin_attachment;
		this.company_logo = company_logo;
		this.phone = phone;
		this.mobile = mobile;
		this.email = email;
		this.website = website;
		this.date_of_incorporation = date_of_incorporation;
		this.financial_year_start = financial_year_start;
		this.book_commencing = book_commencing;
		this.commencement_date = commencement_date;
		this.companyRegistration = companyRegistration;
		this.companyBranch = companyBranch;
		this.companyWarehouse = companyWarehouse;
	}

	public int getCompany_id() {
		return company_id;
	}

	public void setCompany_id(int company_id) {
		this.company_id = company_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTrade_name() {
		return trade_name;
	}

	public void setTrade_name(String trade_name) {
		this.trade_name = trade_name;
	}

	public String getPan() {
		return pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

	public String getPan_effective_date() {
		return pan_effective_date;
	}

	public void setPan_effective_date(String pan_effective_date) {
		this.pan_effective_date = pan_effective_date;
	}

	public String getPan_dep_address() {
		return pan_dep_address;
	}

	public void setPan_dep_address(String pan_dep_address) {
		this.pan_dep_address = pan_dep_address;
	}

	public String getRange_ward() {
		return range_ward;
	}

	public void setRange_ward(String range_ward) {
		this.range_ward = range_ward;
	}

	public String getPan_ph_no() {
		return pan_ph_no;
	}

	public void setPan_ph_no(String pan_ph_no) {
		this.pan_ph_no = pan_ph_no;
	}

	public String getPan_attachment() {
		return pan_attachment;
	}

	public void setPan_attachment(String pan_attachment) {
		this.pan_attachment = pan_attachment;
	}

	public String getOrg_type() {
		return org_type;
	}

	public void setOrg_type(String org_type) {
		this.org_type = org_type;
	}

	public String getBusiness_type() {
		return business_type;
	}

	public void setBusiness_type(String business_type) {
		this.business_type = business_type;
	}

	public String getCompany_crn_no() {
		return company_crn_no;
	}

	public void setCompany_crn_no(String company_crn_no) {
		this.company_crn_no = company_crn_no;
	}

	public String getCrn_eff_date() {
		return crn_eff_date;
	}

	public void setCrn_eff_date(String crn_eff_date) {
		this.crn_eff_date = crn_eff_date;
	}

	public String getCrn_address() {
		return crn_address;
	}

	public void setCrn_address(String crn_address) {
		this.crn_address = crn_address;
	}

	public String getCrn_range_ward() {
		return crn_range_ward;
	}

	public void setCrn_range_ward(String crn_range_ward) {
		this.crn_range_ward = crn_range_ward;
	}

	public String getCrn_ph_no() {
		return crn_ph_no;
	}

	public void setCrn_ph_no(String crn_ph_no) {
		this.crn_ph_no = crn_ph_no;
	}

	public String getCrn_attachment() {
		return crn_attachment;
	}

	public void setCrn_attachment(String crn_attachment) {
		this.crn_attachment = crn_attachment;
	}

	public String getCompany_llpin_no() {
		return company_llpin_no;
	}

	public void setCompany_llpin_no(String company_llpin_no) {
		this.company_llpin_no = company_llpin_no;
	}

	public String getLlpin_eff_date() {
		return llpin_eff_date;
	}

	public void setLlpin_eff_date(String llpin_eff_date) {
		this.llpin_eff_date = llpin_eff_date;
	}

	public String getLlpin_address() {
		return llpin_address;
	}

	public void setLlpin_address(String llpin_address) {
		this.llpin_address = llpin_address;
	}

	public String getLlpin_range_ward() {
		return llpin_range_ward;
	}

	public void setLlpin_range_ward(String llpin_range_ward) {
		this.llpin_range_ward = llpin_range_ward;
	}

	public String getLlpin_ph_no() {
		return llpin_ph_no;
	}

	public void setLlpin_ph_no(String llpin_ph_no) {
		this.llpin_ph_no = llpin_ph_no;
	}

	public String getLlpin_attachment() {
		return llpin_attachment;
	}

	public void setLlpin_attachment(String llpin_attachment) {
		this.llpin_attachment = llpin_attachment;
	}

	public String getCompany_cin_no() {
		return company_cin_no;
	}

	public void setCompany_cin_no(String company_cin_no) {
		this.company_cin_no = company_cin_no;
	}

	public String getCin_eff_date() {
		return cin_eff_date;
	}

	public void setCin_eff_date(String cin_eff_date) {
		this.cin_eff_date = cin_eff_date;
	}

	public String getCin_address() {
		return cin_address;
	}

	public void setCin_address(String cin_address) {
		this.cin_address = cin_address;
	}

	public String getCin_range_ward() {
		return cin_range_ward;
	}

	public void setCin_range_ward(String cin_range_ward) {
		this.cin_range_ward = cin_range_ward;
	}

	public String getCin_ph_no() {
		return cin_ph_no;
	}

	public void setCin_ph_no(String cin_ph_no) {
		this.cin_ph_no = cin_ph_no;
	}

	public String getCin_attachment() {
		return cin_attachment;
	}

	public void setCin_attachment(String cin_attachment) {
		this.cin_attachment = cin_attachment;
	}

	public String getCompany_logo() {
		return company_logo;
	}

	public void setCompany_logo(String company_logo) {
		this.company_logo = company_logo;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getDate_of_incorporation() {
		return date_of_incorporation;
	}

	public void setDate_of_incorporation(String date_of_incorporation) {
		this.date_of_incorporation = date_of_incorporation;
	}

	public String getFinancial_year_start() {
		return financial_year_start;
	}

	public void setFinancial_year_start(String financial_year_start) {
		this.financial_year_start = financial_year_start;
	}

	public String getBook_commencing() {
		return book_commencing;
	}

	public void setBook_commencing(String book_commencing) {
		this.book_commencing = book_commencing;
	}

	public String getCommencement_date() {
		return commencement_date;
	}

	public void setCommencement_date(String commencement_date) {
		this.commencement_date = commencement_date;
	}

	public CompanyRegistration getCompanyRegistration() {
		return companyRegistration;
	}

	public void setCompanyRegistration(CompanyRegistration companyRegistration) {
		this.companyRegistration = companyRegistration;
	}

	public List<CompanyBranch> getCompanyBranch() {
		return companyBranch;
	}

	public void setCompanyBranch(List<CompanyBranch> companyBranch) {
		this.companyBranch = companyBranch;
	}

	public List<CompanyWarehouse> getCompanyWarehouse() {
		return companyWarehouse;
	}

	public void setCompanyWarehouse(List<CompanyWarehouse> companyWarehouse) {
		this.companyWarehouse = companyWarehouse;
	}

	@Override
	public String toString() {
		return "CompanyInfo [company_id=" + company_id + ", name=" + name + ", trade_name=" + trade_name + ", pan="
				+ pan + ", pan_effective_date=" + pan_effective_date + ", pan_dep_address=" + pan_dep_address
				+ ", range_ward=" + range_ward + ", pan_ph_no=" + pan_ph_no + ", pan_attachment=" + pan_attachment
				+ ", org_type=" + org_type + ", business_type=" + business_type + ", company_crn_no=" + company_crn_no
				+ ", crn_eff_date=" + crn_eff_date + ", crn_address=" + crn_address + ", crn_range_ward="
				+ crn_range_ward + ", crn_ph_no=" + crn_ph_no + ", crn_attachment=" + crn_attachment
				+ ", company_llpin_no=" + company_llpin_no + ", llpin_eff_date=" + llpin_eff_date + ", llpin_address="
				+ llpin_address + ", llpin_range_ward=" + llpin_range_ward + ", llpin_ph_no=" + llpin_ph_no
				+ ", llpin_attachment=" + llpin_attachment + ", company_cin_no=" + company_cin_no + ", cin_eff_date="
				+ cin_eff_date + ", cin_address=" + cin_address + ", cin_range_ward=" + cin_range_ward + ", cin_ph_no="
				+ cin_ph_no + ", cin_attachment=" + cin_attachment + ", company_logo=" + company_logo + ", phone="
				+ phone + ", mobile=" + mobile + ", email=" + email + ", website=" + website
				+ ", date_of_incorporation=" + date_of_incorporation + ", financial_year_start=" + financial_year_start
				+ ", book_commencing=" + book_commencing + ", commencement_date=" + commencement_date
				+ ", companyRegistration=" + companyRegistration + ", companyBranch=" + companyBranch
				+ ", companyWarehouse=" + companyWarehouse + "]";
	}

	
	
}
