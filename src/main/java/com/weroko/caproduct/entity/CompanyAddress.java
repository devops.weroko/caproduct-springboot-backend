package com.weroko.caproduct.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
@Entity
public class CompanyAddress {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	private String street;
	
	private String area;
	
	private String city_town;
	
	private int no_of_warehouse;
	
	private String district;
	
	private String police_station;
	
	private String state;
	
	private String country;
	
	private String zip_code;
	
	private int no_of_branches;
	
	@OneToOne
	@JoinColumn(name ="company_id")
	private CompanyInfo companyInfo;

	public CompanyAddress() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CompanyAddress(int id, String street, String area, String city_town, int no_of_warehouse, String district,
			String police_station, String state, String country, String zip_code, int no_of_branches,
			CompanyInfo companyInfo) {
		super();
		this.id = id;
		this.street = street;
		this.area = area;
		this.city_town = city_town;
		this.no_of_warehouse = no_of_warehouse;
		this.district = district;
		this.police_station = police_station;
		this.state = state;
		this.country = country;
		this.zip_code = zip_code;
		this.no_of_branches = no_of_branches;
		this.companyInfo = companyInfo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getCity_town() {
		return city_town;
	}

	public void setCity_town(String city_town) {
		this.city_town = city_town;
	}

	public int getNo_of_warehouse() {
		return no_of_warehouse;
	}

	public void setNo_of_warehouse(int no_of_warehouse) {
		this.no_of_warehouse = no_of_warehouse;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getPolice_station() {
		return police_station;
	}

	public void setPolice_station(String police_station) {
		this.police_station = police_station;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getZip_code() {
		return zip_code;
	}

	public void setZip_code(String zip_code) {
		this.zip_code = zip_code;
	}

	public int getNo_of_branches() {
		return no_of_branches;
	}

	public void setNo_of_branches(int no_of_branches) {
		this.no_of_branches = no_of_branches;
	}

	public CompanyInfo getCompanyInfo() {
		return companyInfo;
	}

	public void setCompanyInfo(CompanyInfo companyInfo) {
		this.companyInfo = companyInfo;
	}

	@Override
	public String toString() {
		return "CompanyAddress [id=" + id + ", street=" + street + ", area=" + area + ", city_town=" + city_town
				+ ", no_of_warehouse=" + no_of_warehouse + ", district=" + district + ", police_station="
				+ police_station + ", state=" + state + ", country=" + country + ", zip_code=" + zip_code
				+ ", no_of_branches=" + no_of_branches + ", companyInfo=" + companyInfo + "]";
	}
	
	


	
	

}
