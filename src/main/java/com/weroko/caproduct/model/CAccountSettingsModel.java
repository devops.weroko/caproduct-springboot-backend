package com.weroko.caproduct.model;

public class CAccountSettingsModel {
	
	private int company_id;
	
	private int id;
	
	private String date_format;
	
	private String base_country;
	
	private String currency_symbol;
	
	private String currency_decimal_place;
	
	private String user_id;
	
	private String password;

	public CAccountSettingsModel() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CAccountSettingsModel(int company_id, int id, String date_format, String base_country,
			String currency_symbol, String currency_decimal_place, String user_id, String password) {
		super();
		this.company_id = company_id;
		this.id = id;
		this.date_format = date_format;
		this.base_country = base_country;
		this.currency_symbol = currency_symbol;
		this.currency_decimal_place = currency_decimal_place;
		this.user_id = user_id;
		this.password = password;
	}

	public int getCompany_id() {
		return company_id;
	}

	public void setCompany_id(int company_id) {
		this.company_id = company_id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDate_format() {
		return date_format;
	}

	public void setDate_format(String date_format) {
		this.date_format = date_format;
	}

	public String getBase_country() {
		return base_country;
	}

	public void setBase_country(String base_country) {
		this.base_country = base_country;
	}

	public String getCurrency_symbol() {
		return currency_symbol;
	}

	public void setCurrency_symbol(String currency_symbol) {
		this.currency_symbol = currency_symbol;
	}

	public String getCurrency_decimal_place() {
		return currency_decimal_place;
	}

	public void setCurrency_decimal_place(String currency_decimal_place) {
		this.currency_decimal_place = currency_decimal_place;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	

}
