package com.weroko.caproduct.model;



public class CompanyAddressModel {



	private int company_id;
	private String street;
	
	private String area;
	
	private String city_town;
	
	private int no_of_warehouse;
	
	private String district;
	
	private String police_station;
	
	private String state;
	
	private String country;
	
	private String zip_code;
	
	private int no_of_branches;
	
	public CompanyAddressModel() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CompanyAddressModel(int company_id, String street, String area, String city_town, int no_of_warehouse,
			String district, String police_station, String state, String country, String zip_code, int no_of_branches) {
		super();
		this.company_id = company_id;
		this.street = street;
		this.area = area;
		this.city_town = city_town;
		this.no_of_warehouse = no_of_warehouse;
		this.district = district;
		this.police_station = police_station;
		this.state = state;
		this.country = country;
		this.zip_code = zip_code;
		this.no_of_branches = no_of_branches;
	}

	public int getCompany_id() {
		return company_id;
	}

	public void setCompany_id(int company_id) {
		this.company_id = company_id;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getCity_town() {
		return city_town;
	}

	public void setCity_town(String city_town) {
		this.city_town = city_town;
	}

	public int getNo_of_warehouse() {
		return no_of_warehouse;
	}

	public void setNo_of_warehouse(int no_of_warehouse) {
		this.no_of_warehouse = no_of_warehouse;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getPolice_station() {
		return police_station;
	}

	public void setPolice_station(String police_station) {
		this.police_station = police_station;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getZip_code() {
		return zip_code;
	}

	public void setZip_code(String zip_code) {
		this.zip_code = zip_code;
	}

	public int getNo_of_branches() {
		return no_of_branches;
	}

	public void setNo_of_branches(int no_of_branches) {
		this.no_of_branches = no_of_branches;
	}
	
	
	

}
