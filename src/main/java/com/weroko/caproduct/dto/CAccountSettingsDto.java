package com.weroko.caproduct.dto;

import com.weroko.caproduct.entity.CompanyInfo;

public class CAccountSettingsDto {
	
	private String date_format;
	
	private String base_country;
	
	private String currency_symbol;
	
	private String currency_decimal_place;
	
	private String user_id;
	
	private String password;
	
	
	private CompanyInfo companyInfo;


	public CAccountSettingsDto() {
		super();
		// TODO Auto-generated constructor stub
	}


	public CAccountSettingsDto(String date_format, String base_country, String currency_symbol,
			String currency_decimal_place, String user_id, String password, CompanyInfo companyInfo) {
		super();
		this.date_format = date_format;
		this.base_country = base_country;
		this.currency_symbol = currency_symbol;
		this.currency_decimal_place = currency_decimal_place;
		this.user_id = user_id;
		this.password = password;
		this.companyInfo = companyInfo;
	}


	public String getDate_format() {
		return date_format;
	}


	public void setDate_format(String date_format) {
		this.date_format = date_format;
	}


	public String getBase_country() {
		return base_country;
	}


	public void setBase_country(String base_country) {
		this.base_country = base_country;
	}


	public String getCurrency_symbol() {
		return currency_symbol;
	}


	public void setCurrency_symbol(String currency_symbol) {
		this.currency_symbol = currency_symbol;
	}


	public String getCurrency_decimal_place() {
		return currency_decimal_place;
	}


	public void setCurrency_decimal_place(String currency_decimal_place) {
		this.currency_decimal_place = currency_decimal_place;
	}


	public String getUser_id() {
		return user_id;
	}


	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public CompanyInfo getCompanyInfo() {
		return companyInfo;
	}


	public void setCompanyInfo(CompanyInfo companyInfo) {
		this.companyInfo = companyInfo;
	}


	@Override
	public String toString() {
		return "CAccountSettingsDto [date_format=" + date_format + ", base_country=" + base_country
				+ ", currency_symbol=" + currency_symbol + ", currency_decimal_place=" + currency_decimal_place
				+ ", user_id=" + user_id + ", password=" + password + ", companyInfo=" + companyInfo + "]";
	}

	
	

}
