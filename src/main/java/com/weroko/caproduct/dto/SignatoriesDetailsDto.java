package com.weroko.caproduct.dto;

public class SignatoriesDetailsDto {
	
	private String name;
	private String father_name;
	private String address;
	private String qualification;
	private String designation;
	private String date_of_birth;
	private String date_of_appointment;
	private String date_of_cessation;
	private String din;
	private String aadhar;
	private String pan;
	private String passport;
	private String aadhar_attachment;
	private String pan_attachment;
	private String passport_attachment;
	private String image_attachment;
	private String authorized_signatories;
	private String share_ratio;
	
	public SignatoriesDetailsDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SignatoriesDetailsDto(String name, String father_name, String address, String qualification,
			String designation, String date_of_birth, String date_of_appointment, String date_of_cessation, String din,
			String aadhar, String pan, String passport, String aadhar_attachment, String pan_attachment,
			String passport_attachment, String image_attachment, String authorized_signatories, String share_ratio) {
		super();
		this.name = name;
		this.father_name = father_name;
		this.address = address;
		this.qualification = qualification;
		this.designation = designation;
		this.date_of_birth = date_of_birth;
		this.date_of_appointment = date_of_appointment;
		this.date_of_cessation = date_of_cessation;
		this.din = din;
		this.aadhar = aadhar;
		this.pan = pan;
		this.passport = passport;
		this.aadhar_attachment = aadhar_attachment;
		this.pan_attachment = pan_attachment;
		this.passport_attachment = passport_attachment;
		this.image_attachment = image_attachment;
		this.authorized_signatories = authorized_signatories;
		this.share_ratio = share_ratio;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFather_name() {
		return father_name;
	}

	public void setFather_name(String father_name) {
		this.father_name = father_name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getQualification() {
		return qualification;
	}

	public void setQualification(String qualification) {
		this.qualification = qualification;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getDate_of_birth() {
		return date_of_birth;
	}

	public void setDate_of_birth(String date_of_birth) {
		this.date_of_birth = date_of_birth;
	}

	public String getDate_of_appointment() {
		return date_of_appointment;
	}

	public void setDate_of_appointment(String date_of_appointment) {
		this.date_of_appointment = date_of_appointment;
	}

	public String getDate_of_cessation() {
		return date_of_cessation;
	}

	public void setDate_of_cessation(String date_of_cessation) {
		this.date_of_cessation = date_of_cessation;
	}

	public String getDin() {
		return din;
	}

	public void setDin(String din) {
		this.din = din;
	}

	public String getAadhar() {
		return aadhar;
	}

	public void setAadhar(String aadhar) {
		this.aadhar = aadhar;
	}

	public String getPan() {
		return pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

	public String getPassport() {
		return passport;
	}

	public void setPassport(String passport) {
		this.passport = passport;
	}

	public String getAadhar_attachment() {
		return aadhar_attachment;
	}

	public void setAadhar_attachment(String aadhar_attachment) {
		this.aadhar_attachment = aadhar_attachment;
	}

	public String getPan_attachment() {
		return pan_attachment;
	}

	public void setPan_attachment(String pan_attachment) {
		this.pan_attachment = pan_attachment;
	}

	public String getPassport_attachment() {
		return passport_attachment;
	}

	public void setPassport_attachment(String passport_attachment) {
		this.passport_attachment = passport_attachment;
	}

	public String getImage_attachment() {
		return image_attachment;
	}

	public void setImage_attachment(String image_attachment) {
		this.image_attachment = image_attachment;
	}

	public String getAuthorized_signatories() {
		return authorized_signatories;
	}

	public void setAuthorized_signatories(String authorized_signatories) {
		this.authorized_signatories = authorized_signatories;
	}

	public String getShare_ratio() {
		return share_ratio;
	}

	public void setShare_ratio(String share_ratio) {
		this.share_ratio = share_ratio;
	}
	
	
	
	

}
