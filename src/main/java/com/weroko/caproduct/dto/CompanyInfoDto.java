package com.weroko.caproduct.dto;

import com.weroko.caproduct.entity.CompanyRegistration;

public class CompanyInfoDto {
	
  
	private int company_id;
	
	private String name;
	
	private String trade_name;
	
	private String pan;
	 
	private String assess_type;
	
	private String business_type;
	
	private String industry_type;
	
	private String msme;
	
	private String company_resg_no;
	
	private String company_cin;
	
	private String cin_ph_no;
	
	private String cin_eff_date;
	
	private String din_dip_address;
	
	private String cin_range_ward;
	
	private String cin_attachment;
	
	private String phone;
	
	private String mobile;
	
	private String email; 
	
	private String website;
	
	private String incop_date;
	
	private String commencement_date;
	
	private String financial_year_start;
	
	private String book_commencing;
	
	private String date_of_incorporation;
	
	private String company_logo;
	
	private String pan_effective_date;
	
	private String pan_dep_address;
	
	private String range_ward;
	
	private String pan_ph_no;

	private String pan_attachment;
	
	private String company_llpin_no;

	private String company_llpin;

	private String llpin_ph_no;

	private String llpin_eff_date;

	private String llpin_range_ward;

	private String llpin_attachment;

	private String company_crn_no;

	private String company_crn;

	private String crn_ph_no;

	private String crn_eff_date;

	private String crn_range_ward;

	private String crn_attachment;


	public CompanyInfoDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CompanyInfoDto(int company_id, String name, String trade_name, String pan, String assess_type,
			String business_type, String industry_type, String msme, String company_resg_no, String company_cin,
			String cin_ph_no, String cin_eff_date, String din_dip_address, String cin_range_ward, String cin_attachment,
			String phone, String mobile, String email, String website, String incop_date, String commencement_date,
			String financial_year_start, String book_commencing, String date_of_incorporation, String company_logo,
			String pan_effective_date, String pan_dep_address, String range_ward, String pan_ph_no,
			String pan_attachment, String company_llpin_no, String company_llpin, String llpin_ph_no,
			String llpin_eff_date, String llpin_range_ward, String llpin_attachment, String company_crn_no,
			String company_crn, String crn_ph_no, String crn_eff_date, String crn_range_ward, String crn_attachment) {
		super();
		
		this.company_id = company_id;
		this.name = name;
		this.trade_name = trade_name;
		this.pan = pan;
		this.assess_type = assess_type;
		this.business_type = business_type;
		this.industry_type = industry_type;
		this.msme = msme;
		this.company_resg_no = company_resg_no;
		this.company_cin = company_cin;
		this.cin_ph_no = cin_ph_no;
		this.cin_eff_date = cin_eff_date;
		this.din_dip_address = din_dip_address;
		this.cin_range_ward = cin_range_ward;
		this.cin_attachment = cin_attachment;
		this.phone = phone;
		this.mobile = mobile;
		this.email = email;
		this.website = website;
		this.incop_date = incop_date;
		this.commencement_date = commencement_date;
		this.financial_year_start = financial_year_start;
		this.book_commencing = book_commencing;
		this.date_of_incorporation = date_of_incorporation;
		this.company_logo = company_logo;
		this.pan_effective_date = pan_effective_date;
		this.pan_dep_address = pan_dep_address;
		this.range_ward = range_ward;
		this.pan_ph_no = pan_ph_no;
		this.pan_attachment = pan_attachment;
		this.company_llpin_no = company_llpin_no;
		this.company_llpin = company_llpin;
		this.llpin_ph_no = llpin_ph_no;
		this.llpin_eff_date = llpin_eff_date;
		this.llpin_range_ward = llpin_range_ward;
		this.llpin_attachment = llpin_attachment;
		this.company_crn_no = company_crn_no;
		this.company_crn = company_crn;
		this.crn_ph_no = crn_ph_no;
		this.crn_eff_date = crn_eff_date;
		this.crn_range_ward = crn_range_ward;
		this.crn_attachment = crn_attachment;
		
	}

	
	public int getCompany_id() {
		return company_id;
	}

	public void setCompany_id(int company_id) {
		this.company_id = company_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTrade_name() {
		return trade_name;
	}

	public void setTrade_name(String trade_name) {
		this.trade_name = trade_name;
	}

	public String getPan() {
		return pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

	public String getAssess_type() {
		return assess_type;
	}

	public void setAssess_type(String assess_type) {
		this.assess_type = assess_type;
	}

	public String getBusiness_type() {
		return business_type;
	}

	public void setBusiness_type(String business_type) {
		this.business_type = business_type;
	}

	public String getIndustry_type() {
		return industry_type;
	}

	public void setIndustry_type(String industry_type) {
		this.industry_type = industry_type;
	}

	public String getMsme() {
		return msme;
	}

	public void setMsme(String msme) {
		this.msme = msme;
	}

	public String getCompany_resg_no() {
		return company_resg_no;
	}

	public void setCompany_resg_no(String company_resg_no) {
		this.company_resg_no = company_resg_no;
	}

	public String getCompany_cin() {
		return company_cin;
	}

	public void setCompany_cin(String company_cin) {
		this.company_cin = company_cin;
	}

	public String getCin_ph_no() {
		return cin_ph_no;
	}

	public void setCin_ph_no(String cin_ph_no) {
		this.cin_ph_no = cin_ph_no;
	}

	public String getCin_eff_date() {
		return cin_eff_date;
	}

	public void setCin_eff_date(String cin_eff_date) {
		this.cin_eff_date = cin_eff_date;
	}

	public String getDin_dip_address() {
		return din_dip_address;
	}

	public void setDin_dip_address(String din_dip_address) {
		this.din_dip_address = din_dip_address;
	}

	public String getCin_range_ward() {
		return cin_range_ward;
	}

	public void setCin_range_ward(String cin_range_ward) {
		this.cin_range_ward = cin_range_ward;
	}

	public String getCin_attachment() {
		return cin_attachment;
	}

	public void setCin_attachment(String cin_attachment) {
		this.cin_attachment = cin_attachment;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getIncop_date() {
		return incop_date;
	}

	public void setIncop_date(String incop_date) {
		this.incop_date = incop_date;
	}

	public String getCommencement_date() {
		return commencement_date;
	}

	public void setCommencement_date(String commencement_date) {
		this.commencement_date = commencement_date;
	}

	public String getFinancial_year_start() {
		return financial_year_start;
	}

	public void setFinancial_year_start(String financial_year_start) {
		this.financial_year_start = financial_year_start;
	}

	public String getBook_commencing() {
		return book_commencing;
	}

	public void setBook_commencing(String book_commencing) {
		this.book_commencing = book_commencing;
	}

	public String getDate_of_incorporation() {
		return date_of_incorporation;
	}

	public void setDate_of_incorporation(String date_of_incorporation) {
		this.date_of_incorporation = date_of_incorporation;
	}

	public String getCompany_logo() {
		return company_logo;
	}

	public void setCompany_logo(String company_logo) {
		this.company_logo = company_logo;
	}

	public String getPan_effective_date() {
		return pan_effective_date;
	}

	public void setPan_effective_date(String pan_effective_date) {
		this.pan_effective_date = pan_effective_date;
	}

	public String getPan_dep_address() {
		return pan_dep_address;
	}

	public void setPan_dep_address(String pan_dep_address) {
		this.pan_dep_address = pan_dep_address;
	}

	public String getRange_ward() {
		return range_ward;
	}

	public void setRange_ward(String range_ward) {
		this.range_ward = range_ward;
	}

	public String getPan_ph_no() {
		return pan_ph_no;
	}

	public void setPan_ph_no(String pan_ph_no) {
		this.pan_ph_no = pan_ph_no;
	}

	public String getPan_attachment() {
		return pan_attachment;
	}

	public void setPan_attachment(String pan_attachment) {
		this.pan_attachment = pan_attachment;
	}

	public String getCompany_llpin_no() {
		return company_llpin_no;
	}

	public void setCompany_llpin_no(String company_llpin_no) {
		this.company_llpin_no = company_llpin_no;
	}

	public String getCompany_llpin() {
		return company_llpin;
	}

	public void setCompany_llpin(String company_llpin) {
		this.company_llpin = company_llpin;
	}

	public String getLlpin_ph_no() {
		return llpin_ph_no;
	}

	public void setLlpin_ph_no(String llpin_ph_no) {
		this.llpin_ph_no = llpin_ph_no;
	}

	public String getLlpin_eff_date() {
		return llpin_eff_date;
	}

	public void setLlpin_eff_date(String llpin_eff_date) {
		this.llpin_eff_date = llpin_eff_date;
	}

	public String getLlpin_range_ward() {
		return llpin_range_ward;
	}

	public void setLlpin_range_ward(String llpin_range_ward) {
		this.llpin_range_ward = llpin_range_ward;
	}

	public String getLlpin_attachment() {
		return llpin_attachment;
	}

	public void setLlpin_attachment(String llpin_attachment) {
		this.llpin_attachment = llpin_attachment;
	}

	public String getCompany_crn_no() {
		return company_crn_no;
	}

	public void setCompany_crn_no(String company_crn_no) {
		this.company_crn_no = company_crn_no;
	}

	public String getCompany_crn() {
		return company_crn;
	}

	public void setCompany_crn(String company_crn) {
		this.company_crn = company_crn;
	}

	public String getCrn_ph_no() {
		return crn_ph_no;
	}

	public void setCrn_ph_no(String crn_ph_no) {
		this.crn_ph_no = crn_ph_no;
	}

	public String getCrn_eff_date() {
		return crn_eff_date;
	}

	public void setCrn_eff_date(String crn_eff_date) {
		this.crn_eff_date = crn_eff_date;
	}

	public String getCrn_range_ward() {
		return crn_range_ward;
	}

	public void setCrn_range_ward(String crn_range_ward) {
		this.crn_range_ward = crn_range_ward;
	}

	public String getCrn_attachment() {
		return crn_attachment;
	}

	public void setCrn_attachment(String crn_attachment) {
		this.crn_attachment = crn_attachment;
	}



	@Override
	public String toString() {
		return "CompanyInfoDto [ name=" + name + ", trade_name=" + trade_name + ", pan="
				+ pan + ", assess_type=" + assess_type + ", business_type=" + business_type + ", industry_type="
				+ industry_type + ", msme=" + msme + ", company_resg_no=" + company_resg_no + ", company_cin="
				+ company_cin + ", cin_ph_no=" + cin_ph_no + ", cin_eff_date=" + cin_eff_date + ", din_dip_address="
				+ din_dip_address + ", cin_range_ward=" + cin_range_ward + ", cin_attachment=" + cin_attachment
				+ ", phone=" + phone + ", mobile=" + mobile + ", email=" + email + ", website=" + website
				+ ", incop_date=" + incop_date + ", commencement_date=" + commencement_date + ", financial_year_start="
				+ financial_year_start + ", book_commencing=" + book_commencing + ", date_of_incorporation="
				+ date_of_incorporation + ", company_logo=" + company_logo + ", pan_effective_date="
				+ pan_effective_date + ", pan_dep_address=" + pan_dep_address + ", range_ward=" + range_ward
				+ ", pan_ph_no=" + pan_ph_no + ", pan_attachment=" + pan_attachment + ", company_llpin_no="
				+ company_llpin_no + ", company_llpin=" + company_llpin + ", llpin_ph_no=" + llpin_ph_no
				+ ", llpin_eff_date=" + llpin_eff_date + ", llpin_range_ward=" + llpin_range_ward
				+ ", llpin_attachment=" + llpin_attachment + ", company_crn_no=" + company_crn_no + ", company_crn="
				+ company_crn + ", crn_ph_no=" + crn_ph_no + ", crn_eff_date=" + crn_eff_date + ", crn_range_ward="
				+ crn_range_ward + ", crn_attachment=" + crn_attachment 
				+ "]";
	}
	
	


}
