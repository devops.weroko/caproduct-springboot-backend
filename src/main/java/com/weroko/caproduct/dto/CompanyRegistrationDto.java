package com.weroko.caproduct.dto;

import java.util.List;

import com.weroko.caproduct.entity.CompanyInfo;

public class CompanyRegistrationDto {
	
    private String name;
	
	private String contact_no;
	
	private String username;
	
	private String firm_name;
	
	private String company_type;
	
	private String user_type;
	
	private String register_for;
	
	private String password;
	
	private List<CompanyInfo> companyInfos;

	public CompanyRegistrationDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CompanyRegistrationDto(String name, String contact_no, String username, String firm_name,
			String company_type, String user_type, String register_for, String password,
			List<CompanyInfo> companyInfos) {
		super();
		this.name = name;
		this.contact_no = contact_no;
		this.username = username;
		this.firm_name = firm_name;
		this.company_type = company_type;
		this.user_type = user_type;
		this.register_for = register_for;
		this.password = password;
		this.companyInfos = companyInfos;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContact_no() {
		return contact_no;
	}

	public void setContact_no(String contact_no) {
		this.contact_no = contact_no;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFirm_name() {
		return firm_name;
	}

	public void setFirm_name(String firm_name) {
		this.firm_name = firm_name;
	}

	public String getCompany_type() {
		return company_type;
	}

	public void setCompany_type(String company_type) {
		this.company_type = company_type;
	}

	public String getUser_type() {
		return user_type;
	}

	public void setUser_type(String user_type) {
		this.user_type = user_type;
	}

	public String getRegister_for() {
		return register_for;
	}

	public void setRegister_for(String register_for) {
		this.register_for = register_for;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<CompanyInfo> getCompanyInfos() {
		return companyInfos;
	}

	public void setCompanyInfos(List<CompanyInfo> companyInfos) {
		this.companyInfos = companyInfos;
	}

	
	

}
