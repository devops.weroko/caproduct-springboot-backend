package com.weroko.caproduct.dto;

public class UserInfo {
	
	private int user_id;
	private String name;
	private String profileImage;
	

	public UserInfo() {
		super();
		// TODO Auto-generated constructor stub
	}


	public UserInfo(int user_id, String name, String profileImage) {
		super();
		this.user_id = user_id;
		this.name = name;
		this.profileImage = profileImage;
	}


	public int getUser_id() {
		return user_id;
	}


	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getProfileImage() {
		return profileImage;
	}


	public void setProfileImage(String profileImage) {
		this.profileImage = profileImage;
	}

	
	
	

}
