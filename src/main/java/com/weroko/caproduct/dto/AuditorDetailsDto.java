package com.weroko.caproduct.dto;

public class AuditorDetailsDto {
	
	//Tax Auditor

    private String tax_name_of_auditor;
    private String tax_auditors_pan;
    private String tax_membership_no;
    private String tax_address;
    private String tax_country;
    private String tax_state;
    private String tax_zip_code;
    private String tax_firm_name;
    private String tax_firm_pan;
    private String tax_frn;
    private String tax_phone;
    private String tax_email;
    private String tax_period_from;
    private String tax_to;

    // GST auditor

    private String gst_name_of_auditor;
    private String gst_auditors_pan;
    private String gst_membership_no;
    private String gst_address;
    private String gst_country;
    private String gst_state;
    private String gst_zip_code;
    private String gst_firm_name;
    private String gst_firm_pan;
    private String gst_frn;
    private String gst_phone;
    private String gst_email;
    private String gst_period_from;
    private String gst_to;

    //Satutory Auditor Details

    private String satutory_name_of_auditor;
    private String satutory_auditors_pan;
    private String satutory_membership_no;
    private String satutory_address;
    private String satutory_country;
    private String satutory_state;
    private String satutory_zip_code;
    private String satutory_firm_name;
    private String satutory_firm_pan;
    private String satutory_frn;
    private String satutory_phone;
    private String satutory_email;
    private String satutory_period_from;
    private String satutory_to;
	
    public AuditorDetailsDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AuditorDetailsDto(String tax_name_of_auditor, String tax_auditors_pan, String tax_membership_no,
			String tax_address, String tax_country, String tax_state, String tax_zip_code, String tax_firm_name,
			String tax_firm_pan, String tax_frn, String tax_phone, String tax_email, String tax_period_from,
			String tax_to, String gst_name_of_auditor, String gst_auditors_pan, String gst_membership_no,
			String gst_address, String gst_country, String gst_state, String gst_zip_code, String gst_firm_name,
			String gst_firm_pan, String gst_frn, String gst_phone, String gst_email, String gst_period_from,
			String gst_to, String satutory_name_of_auditor, String satutory_auditors_pan, String satutory_membership_no,
			String satutory_address, String satutory_country, String satutory_state, String satutory_zip_code,
			String satutory_firm_name, String satutory_firm_pan, String satutory_frn, String satutory_phone,
			String satutory_email, String satutory_period_from, String satutory_to) {
		super();
		this.tax_name_of_auditor = tax_name_of_auditor;
		this.tax_auditors_pan = tax_auditors_pan;
		this.tax_membership_no = tax_membership_no;
		this.tax_address = tax_address;
		this.tax_country = tax_country;
		this.tax_state = tax_state;
		this.tax_zip_code = tax_zip_code;
		this.tax_firm_name = tax_firm_name;
		this.tax_firm_pan = tax_firm_pan;
		this.tax_frn = tax_frn;
		this.tax_phone = tax_phone;
		this.tax_email = tax_email;
		this.tax_period_from = tax_period_from;
		this.tax_to = tax_to;
		this.gst_name_of_auditor = gst_name_of_auditor;
		this.gst_auditors_pan = gst_auditors_pan;
		this.gst_membership_no = gst_membership_no;
		this.gst_address = gst_address;
		this.gst_country = gst_country;
		this.gst_state = gst_state;
		this.gst_zip_code = gst_zip_code;
		this.gst_firm_name = gst_firm_name;
		this.gst_firm_pan = gst_firm_pan;
		this.gst_frn = gst_frn;
		this.gst_phone = gst_phone;
		this.gst_email = gst_email;
		this.gst_period_from = gst_period_from;
		this.gst_to = gst_to;
		this.satutory_name_of_auditor = satutory_name_of_auditor;
		this.satutory_auditors_pan = satutory_auditors_pan;
		this.satutory_membership_no = satutory_membership_no;
		this.satutory_address = satutory_address;
		this.satutory_country = satutory_country;
		this.satutory_state = satutory_state;
		this.satutory_zip_code = satutory_zip_code;
		this.satutory_firm_name = satutory_firm_name;
		this.satutory_firm_pan = satutory_firm_pan;
		this.satutory_frn = satutory_frn;
		this.satutory_phone = satutory_phone;
		this.satutory_email = satutory_email;
		this.satutory_period_from = satutory_period_from;
		this.satutory_to = satutory_to;
	}

	public String getTax_name_of_auditor() {
		return tax_name_of_auditor;
	}

	public void setTax_name_of_auditor(String tax_name_of_auditor) {
		this.tax_name_of_auditor = tax_name_of_auditor;
	}

	public String getTax_auditors_pan() {
		return tax_auditors_pan;
	}

	public void setTax_auditors_pan(String tax_auditors_pan) {
		this.tax_auditors_pan = tax_auditors_pan;
	}

	public String getTax_membership_no() {
		return tax_membership_no;
	}

	public void setTax_membership_no(String tax_membership_no) {
		this.tax_membership_no = tax_membership_no;
	}

	public String getTax_address() {
		return tax_address;
	}

	public void setTax_address(String tax_address) {
		this.tax_address = tax_address;
	}

	public String getTax_country() {
		return tax_country;
	}

	public void setTax_country(String tax_country) {
		this.tax_country = tax_country;
	}

	public String getTax_state() {
		return tax_state;
	}

	public void setTax_state(String tax_state) {
		this.tax_state = tax_state;
	}

	public String getTax_zip_code() {
		return tax_zip_code;
	}

	public void setTax_zip_code(String tax_zip_code) {
		this.tax_zip_code = tax_zip_code;
	}

	public String getTax_firm_name() {
		return tax_firm_name;
	}

	public void setTax_firm_name(String tax_firm_name) {
		this.tax_firm_name = tax_firm_name;
	}

	public String getTax_firm_pan() {
		return tax_firm_pan;
	}

	public void setTax_firm_pan(String tax_firm_pan) {
		this.tax_firm_pan = tax_firm_pan;
	}

	public String getTax_frn() {
		return tax_frn;
	}

	public void setTax_frn(String tax_frn) {
		this.tax_frn = tax_frn;
	}

	public String getTax_phone() {
		return tax_phone;
	}

	public void setTax_phone(String tax_phone) {
		this.tax_phone = tax_phone;
	}

	public String getTax_email() {
		return tax_email;
	}

	public void setTax_email(String tax_email) {
		this.tax_email = tax_email;
	}

	public String getTax_period_from() {
		return tax_period_from;
	}

	public void setTax_period_from(String tax_period_from) {
		this.tax_period_from = tax_period_from;
	}

	public String getTax_to() {
		return tax_to;
	}

	public void setTax_to(String tax_to) {
		this.tax_to = tax_to;
	}

	public String getGst_name_of_auditor() {
		return gst_name_of_auditor;
	}

	public void setGst_name_of_auditor(String gst_name_of_auditor) {
		this.gst_name_of_auditor = gst_name_of_auditor;
	}

	public String getGst_auditors_pan() {
		return gst_auditors_pan;
	}

	public void setGst_auditors_pan(String gst_auditors_pan) {
		this.gst_auditors_pan = gst_auditors_pan;
	}

	public String getGst_membership_no() {
		return gst_membership_no;
	}

	public void setGst_membership_no(String gst_membership_no) {
		this.gst_membership_no = gst_membership_no;
	}

	public String getGst_address() {
		return gst_address;
	}

	public void setGst_address(String gst_address) {
		this.gst_address = gst_address;
	}

	public String getGst_country() {
		return gst_country;
	}

	public void setGst_country(String gst_country) {
		this.gst_country = gst_country;
	}

	public String getGst_state() {
		return gst_state;
	}

	public void setGst_state(String gst_state) {
		this.gst_state = gst_state;
	}

	public String getGst_zip_code() {
		return gst_zip_code;
	}

	public void setGst_zip_code(String gst_zip_code) {
		this.gst_zip_code = gst_zip_code;
	}

	public String getGst_firm_name() {
		return gst_firm_name;
	}

	public void setGst_firm_name(String gst_firm_name) {
		this.gst_firm_name = gst_firm_name;
	}

	public String getGst_firm_pan() {
		return gst_firm_pan;
	}

	public void setGst_firm_pan(String gst_firm_pan) {
		this.gst_firm_pan = gst_firm_pan;
	}

	public String getGst_frn() {
		return gst_frn;
	}

	public void setGst_frn(String gst_frn) {
		this.gst_frn = gst_frn;
	}

	public String getGst_phone() {
		return gst_phone;
	}

	public void setGst_phone(String gst_phone) {
		this.gst_phone = gst_phone;
	}

	public String getGst_email() {
		return gst_email;
	}

	public void setGst_email(String gst_email) {
		this.gst_email = gst_email;
	}

	public String getGst_period_from() {
		return gst_period_from;
	}

	public void setGst_period_from(String gst_period_from) {
		this.gst_period_from = gst_period_from;
	}

	public String getGst_to() {
		return gst_to;
	}

	public void setGst_to(String gst_to) {
		this.gst_to = gst_to;
	}

	public String getSatutory_name_of_auditor() {
		return satutory_name_of_auditor;
	}

	public void setSatutory_name_of_auditor(String satutory_name_of_auditor) {
		this.satutory_name_of_auditor = satutory_name_of_auditor;
	}

	public String getSatutory_auditors_pan() {
		return satutory_auditors_pan;
	}

	public void setSatutory_auditors_pan(String satutory_auditors_pan) {
		this.satutory_auditors_pan = satutory_auditors_pan;
	}

	public String getSatutory_membership_no() {
		return satutory_membership_no;
	}

	public void setSatutory_membership_no(String satutory_membership_no) {
		this.satutory_membership_no = satutory_membership_no;
	}

	public String getSatutory_address() {
		return satutory_address;
	}

	public void setSatutory_address(String satutory_address) {
		this.satutory_address = satutory_address;
	}

	public String getSatutory_country() {
		return satutory_country;
	}

	public void setSatutory_country(String satutory_country) {
		this.satutory_country = satutory_country;
	}

	public String getSatutory_state() {
		return satutory_state;
	}

	public void setSatutory_state(String satutory_state) {
		this.satutory_state = satutory_state;
	}

	public String getSatutory_zip_code() {
		return satutory_zip_code;
	}

	public void setSatutory_zip_code(String satutory_zip_code) {
		this.satutory_zip_code = satutory_zip_code;
	}

	public String getSatutory_firm_name() {
		return satutory_firm_name;
	}

	public void setSatutory_firm_name(String satutory_firm_name) {
		this.satutory_firm_name = satutory_firm_name;
	}

	public String getSatutory_firm_pan() {
		return satutory_firm_pan;
	}

	public void setSatutory_firm_pan(String satutory_firm_pan) {
		this.satutory_firm_pan = satutory_firm_pan;
	}

	public String getSatutory_frn() {
		return satutory_frn;
	}

	public void setSatutory_frn(String satutory_frn) {
		this.satutory_frn = satutory_frn;
	}

	public String getSatutory_phone() {
		return satutory_phone;
	}

	public void setSatutory_phone(String satutory_phone) {
		this.satutory_phone = satutory_phone;
	}

	public String getSatutory_email() {
		return satutory_email;
	}

	public void setSatutory_email(String satutory_email) {
		this.satutory_email = satutory_email;
	}

	public String getSatutory_period_from() {
		return satutory_period_from;
	}

	public void setSatutory_period_from(String satutory_period_from) {
		this.satutory_period_from = satutory_period_from;
	}

	public String getSatutory_to() {
		return satutory_to;
	}

	public void setSatutory_to(String satutory_to) {
		this.satutory_to = satutory_to;
	}
    
    
    
    

}
