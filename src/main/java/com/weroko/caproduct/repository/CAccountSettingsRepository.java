package com.weroko.caproduct.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.weroko.caproduct.entity.CAccountSettings;
@Repository
public interface CAccountSettingsRepository extends JpaRepository<CAccountSettings, Integer> {

}
