package com.weroko.caproduct.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.weroko.caproduct.entity.CompanyAddress;
import com.weroko.caproduct.entity.CompanyBranch;
@Repository
public interface CompanyBranchRepository extends JpaRepository<CompanyBranch, Integer> {

}
