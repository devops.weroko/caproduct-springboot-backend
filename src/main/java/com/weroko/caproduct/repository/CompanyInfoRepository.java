package com.weroko.caproduct.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.weroko.caproduct.entity.CompanyInfo;
@Repository
public interface CompanyInfoRepository extends JpaRepository<CompanyInfo, Integer>{

}
