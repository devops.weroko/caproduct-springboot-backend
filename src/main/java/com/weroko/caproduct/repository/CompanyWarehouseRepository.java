package com.weroko.caproduct.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.weroko.caproduct.entity.CompanyInfo;
import com.weroko.caproduct.entity.CompanyWarehouse;
@Repository
public interface CompanyWarehouseRepository extends JpaRepository<CompanyWarehouse, Integer>{

}
