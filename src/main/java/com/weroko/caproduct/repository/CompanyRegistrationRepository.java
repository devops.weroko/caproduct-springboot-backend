package com.weroko.caproduct.repository;

import org.hibernate.validator.constraints.pl.REGON;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.weroko.caproduct.entity.CompanyRegistration;
@Repository
public interface CompanyRegistrationRepository extends JpaRepository<CompanyRegistration, Integer>{
	
	CompanyRegistration findByUsername(String username);

}
