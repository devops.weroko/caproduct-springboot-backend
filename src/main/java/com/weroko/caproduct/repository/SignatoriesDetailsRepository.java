package com.weroko.caproduct.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.weroko.caproduct.entity.SignatoriesDetails;

@Repository
public interface SignatoriesDetailsRepository extends JpaRepository<SignatoriesDetails, Integer> {

}
