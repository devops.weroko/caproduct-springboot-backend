package com.weroko.caproduct.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.weroko.caproduct.entity.AuditorDetails;

@Repository
public interface AuditorDetailsRepository extends JpaRepository<AuditorDetails, Integer>{

}
