package com.weroko.caproduct;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class CaproductApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(CaproductApplication.class, args);
	}

}
