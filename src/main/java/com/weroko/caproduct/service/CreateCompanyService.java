package com.weroko.caproduct.service;

import com.weroko.caproduct.dto.AuditorDetailsDto;
import com.weroko.caproduct.dto.CAccountSettingsDto;
import com.weroko.caproduct.dto.CompanyAddressDto;
import com.weroko.caproduct.dto.CompanyInfoDto;
import com.weroko.caproduct.dto.CompanyRegistrationDto;
import com.weroko.caproduct.dto.SignatoriesDetailsDto;
import com.weroko.caproduct.model.CompanyAddressModel;

public interface CreateCompanyService {

	
	

	CompanyInfoDto addCompanyInfo(CompanyInfoDto companyInfoDto, int user_id);

	CompanyAddressModel addCompanyAddress(CompanyAddressModel companyAddressModel);

	String addCompanySettings(CAccountSettingsDto cAccountSettingsDto, int company_id);

	String addSignatoriesDetails(SignatoriesDetailsDto signatoriesDetailsDto, int company_id);

	String addAuditorDetails(AuditorDetailsDto auditorDetailsDto, int company_id);

}
