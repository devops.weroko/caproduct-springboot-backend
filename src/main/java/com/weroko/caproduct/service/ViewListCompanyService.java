package com.weroko.caproduct.service;

import java.util.List;

import com.weroko.caproduct.entity.CompanyAddress;
import com.weroko.caproduct.entity.CompanyInfo;
import com.weroko.caproduct.model.CompanyAddressModel;
import com.weroko.caproduct.model.CompanyInfoModel;

public interface ViewListCompanyService {

	List<CompanyInfo> getCompanyList(int user_id);

	CompanyInfoModel getCompanyInfo(int company_id);

	CompanyAddressModel getCompanyAddress(int company_id);

	

}
