package com.weroko.caproduct.service.serviceimpl;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.weroko.caproduct.entity.CompanyAddress;
import com.weroko.caproduct.entity.CompanyInfo;
import com.weroko.caproduct.model.CompanyAddressModel;
import com.weroko.caproduct.model.CompanyInfoModel;
import com.weroko.caproduct.repository.CompanyAddressRepository;
import com.weroko.caproduct.repository.CompanyInfoRepository;
import com.weroko.caproduct.service.ViewListCompanyService;

@Service
public class ViewListCompanyServiceImpl implements ViewListCompanyService {

	@Autowired
	CompanyInfoRepository companyInfoRepository;

	@Autowired
	CompanyAddressRepository companyAddressRepository;
	
	ModelMapper modelMapper= new ModelMapper();

	@Override
	public List<CompanyInfo> getCompanyList(int user_id) {
		// TODO Auto-generated method stub

		List<CompanyInfo> companyInfos = companyInfoRepository.findAll();
		List<CompanyInfo> companyInfos2 = new ArrayList<CompanyInfo>();
		for (CompanyInfo companyInfo : companyInfos) {
			if (companyInfo.getCompanyRegistration().getUser_id() == user_id) {
				companyInfos2.add(companyInfo);

			}
		}

		return companyInfos2;

		/*
		 * CompanyInfo companyInfo = new CompanyInfo(); companyInfo.setName("Martin");
		 * //companyInfo.setCompanyRegistration().setUser_id(1); CompanyInfo
		 * companyInfo2 = new CompanyInfo(); companyInfo2.setName("Martin2");
		 * List<CompanyInfo> companyInfos2 = new ArrayList<CompanyInfo>();
		 * companyInfos2.add(companyInfo); companyInfos2.add(companyInfo2); return
		 * companyInfos2;
		 */
	}

	@Override
	public CompanyInfoModel getCompanyInfo(int company_id) {
		CompanyInfo companyInfo = companyInfoRepository.findById(company_id).get();
		CompanyInfoModel companyInfoModel =  modelMapper.map(companyInfo, CompanyInfoModel.class);
		return companyInfoModel;
	}

	@Override
	public CompanyAddressModel getCompanyAddress(int company_id) {
//		CompanyInfo companyInfo = companyInfoRepository.findById(company_id).get();
		CompanyAddress companyAddress = new CompanyAddress();

//		// Way_1
//		List<CompanyAddress> companyAddressList = new ArrayList<CompanyAddress>();
//		companyAddressList = companyAddressRepository.findAll();
//		for (CompanyAddress companyAddress2 : companyAddressList) {
//			if (companyAddress2.getCompanyInfo().getCompany_id() == company_id) {
//				companyAddress = companyAddress2;
//			}
//		}

		// Way_2
		companyAddress = companyAddressRepository.findByCompanyId(company_id);
		CompanyAddressModel companyAddressModel =  modelMapper.map(companyAddress, CompanyAddressModel.class);

		return companyAddressModel;
	}

}
