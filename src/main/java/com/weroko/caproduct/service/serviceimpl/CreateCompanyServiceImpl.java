package com.weroko.caproduct.service.serviceimpl;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.weroko.caproduct.dto.AuditorDetailsDto;
import com.weroko.caproduct.dto.CAccountSettingsDto;
import com.weroko.caproduct.dto.CompanyAddressDto;
import com.weroko.caproduct.dto.CompanyInfoDto;
import com.weroko.caproduct.dto.CompanyRegistrationDto;
import com.weroko.caproduct.dto.SignatoriesDetailsDto;
import com.weroko.caproduct.entity.AuditorDetails;
import com.weroko.caproduct.entity.CAccountSettings;
import com.weroko.caproduct.entity.CompanyAddress;
import com.weroko.caproduct.entity.CompanyInfo;
import com.weroko.caproduct.entity.CompanyRegistration;
import com.weroko.caproduct.entity.SignatoriesDetails;
import com.weroko.caproduct.model.CompanyAddressModel;
import com.weroko.caproduct.model.CompanyInfoModel;
import com.weroko.caproduct.repository.AuditorDetailsRepository;
import com.weroko.caproduct.repository.CAccountSettingsRepository;
import com.weroko.caproduct.repository.CompanyAddressRepository;
import com.weroko.caproduct.repository.CompanyInfoRepository;
import com.weroko.caproduct.repository.CompanyRegistrationRepository;
import com.weroko.caproduct.repository.SignatoriesDetailsRepository;
import com.weroko.caproduct.service.CreateCompanyService;
@Service
public class CreateCompanyServiceImpl  implements CreateCompanyService{
	
	@Autowired
	CompanyInfoRepository companyInfoRepository;
	
	@Autowired
	CompanyAddressRepository companyAddressRepository;
	
	@Autowired
	CompanyRegistrationRepository companyRegistrationRepository;
	
	@Autowired
	CAccountSettingsRepository cAccountSettingsRepository;
	
	@Autowired
	SignatoriesDetailsRepository signatoriesDetailsRepository;
	
	@Autowired
	AuditorDetailsRepository auditorDetailsRepository;
	
	
	ModelMapper modelMapper= new ModelMapper();

	@Override
	public CompanyInfoDto addCompanyInfo(CompanyInfoDto companyInfoDto, int user_id) {
		CompanyInfo companyInfo= modelMapper.map(companyInfoDto, CompanyInfo.class);
		System.out.println("starting");
		System.out.println(companyInfo);
		
		CompanyRegistration companyRegistration=companyRegistrationRepository.findById(user_id).get();
		
	companyInfo.setCompanyRegistration(companyRegistration);

	
	CompanyInfo companyInfo2=companyInfoRepository.save(companyInfo);
//	if (companyInfo2!=null) 
//		return "Successfully inserted!";
//	else
//	return "Some exception has occured";
	CompanyInfoDto companyInfoDto2 =  modelMapper.map(companyInfo2, CompanyInfoDto.class);

	return companyInfoDto2;
	
	}

	

	@Override
	public CompanyAddressModel addCompanyAddress(CompanyAddressModel companyAddressModel) {
		
		CompanyAddress companyAddress=modelMapper.map(companyAddressModel, CompanyAddress.class);
		
		CompanyInfo companyInfo=companyInfoRepository.findById(companyAddressModel.getCompany_id()).get();
		companyAddress.setCompanyInfo(companyInfo);
		CompanyAddress companyAddress2=companyAddressRepository.save(companyAddress);
		
//		if (companyAddress2!=null) {
//			return "Successfully added company address!";
//		}
//		else
//		return "Error occured at addCompanyAddress!";
		
		CompanyAddressModel companyAddressModel2 =  modelMapper.map(companyAddress2, CompanyAddressModel.class);
		
		return companyAddressModel2;
	}

	@Override
	public String addCompanySettings(CAccountSettingsDto cAccountSettingsDto, int company_id) {
		CAccountSettings cAccountSettings= modelMapper.map(cAccountSettingsDto, CAccountSettings.class);
		
		CompanyInfo companyInfo=companyInfoRepository.findById(company_id).get();
		cAccountSettings.setCompanyInfo(companyInfo);
		CAccountSettings cAccountSettings2=cAccountSettingsRepository.save(cAccountSettings);
		
		if (cAccountSettings2!=null) {
			return "Successfully added CA account settings!";
		}
		else
		return "Error occured at CA Company Settings!";
	}



	@Override
	public String addSignatoriesDetails(SignatoriesDetailsDto signatoriesDetailsDto, int company_id) {
		
		
		SignatoriesDetails signatoriesDetails = modelMapper.map(signatoriesDetailsDto, SignatoriesDetails.class);
		CompanyInfo companyInfo=companyInfoRepository.findById(company_id).get();
		signatoriesDetails.setCompanyInfo(companyInfo);
		
		SignatoriesDetails signatoriesDetails2= signatoriesDetailsRepository.save(signatoriesDetails);
		
		if (signatoriesDetails2!=null) {
			return "Successfully added Signatories Details!";
		}
		else
		return "Error occured at Signatories Details!";
	}



	@Override
	public String addAuditorDetails(AuditorDetailsDto auditorDetailsDto, int company_id) {
		
		
		AuditorDetails auditorDetails = modelMapper.map(auditorDetailsDto, AuditorDetails.class);
		CompanyInfo companyInfo=companyInfoRepository.findById(company_id).get();
		auditorDetails.setCompanyInfo(companyInfo);
		
		AuditorDetails auditorDetails2= auditorDetailsRepository.save(auditorDetails);
		
		if (auditorDetails2!=null) {
			return "Successfully added Auditor Details!";
		}
		else
		return "Error occured at Auditor Details!";
		
	}
	
	



	

	

}
