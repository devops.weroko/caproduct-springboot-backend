package com.weroko.caproduct.service.serviceimpl;

import java.util.ArrayList;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.weroko.caproduct.dto.CompanyRegistrationDto;
import com.weroko.caproduct.dto.UserInfo;
import com.weroko.caproduct.entity.CompanyRegistration;
import com.weroko.caproduct.repository.CompanyRegistrationRepository;
import com.weroko.caproduct.service.HomeControllerService;

@Service
public class HomeControllerServiceImpl implements HomeControllerService, UserDetailsService {
	
	@Autowired
	CompanyRegistrationRepository companyRegistrationRepository;
	
	@Autowired
	private PasswordEncoder bcryptEncoder;
	
	ModelMapper modelMapper= new ModelMapper();

	@Override
	public String registerUser(CompanyRegistrationDto companyRegistrationDto) {
		
		CompanyRegistration companyRegistration= modelMapper.map(companyRegistrationDto, CompanyRegistration.class);
		
		companyRegistration.setPassword(bcryptEncoder.encode(companyRegistrationDto.getPassword()));
		
		CompanyRegistration companyRegistration2= companyRegistrationRepository.save(companyRegistration);
		if (companyRegistration2!=null) 
			return "Redirecting to login...";
		else
		return "Error occured!";
	}
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		CompanyRegistration user = companyRegistrationRepository.findByUsername(username);
		if (user == null) {
			throw new UsernameNotFoundException("Invalid username/password");
		}
		return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
				new ArrayList<>());
	}

	@Override
	public UserInfo getAllUserDetails(String username) {
		CompanyRegistration user = companyRegistrationRepository.findByUsername(username);
		UserInfo userInfo = new UserInfo();
		userInfo.setName(user.getName());
		userInfo.setUser_id(user.getUser_id());
		
		return userInfo;
	}
	
}
