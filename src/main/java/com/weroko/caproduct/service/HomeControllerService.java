package com.weroko.caproduct.service;

import org.springframework.security.core.userdetails.UserDetails;

import com.weroko.caproduct.dto.CompanyRegistrationDto;
import com.weroko.caproduct.dto.UserInfo;
import com.weroko.caproduct.exception.ServiceException;

public interface HomeControllerService {

	String registerUser(CompanyRegistrationDto companyRegistrationDto);

	UserDetails loadUserByUsername(String username);

	UserInfo getAllUserDetails(String username); 

}
